FROM golang:1.19.2-alpine as builder
RUN apk add --no-cache ca-certificates git
RUN apk add build-base
WORKDIR /app

ARG VERSION

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build -ldflags "-X main.version=$VERSION" -o ci-linter ./cmd/main.go

FROM alpine as release
LABEL maintainer="halmad"
RUN apk add --no-cache ca-certificates
WORKDIR /app

COPY --from=builder /app/ci-linter ci-linter

RUN cp /app/ci-linter /usr/bin/ci-linter
ENTRYPOINT [ "ci-linter" ]
