package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"gopkg.in/yaml.v3"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type ValidationResult struct {
	yamlFile string
	ok       bool
	err      error
}

type ValidationResponse struct {
	Valid    bool          `json:"valid"`
	Errors   []interface{} `json:"errors"`
	Warnings []interface{} `json:"warnings"`
	Includes []interface{} `json:"includes"`
	Status   string        `json:"status"`
}

type arrayFlags []string

func (i *arrayFlags) String() string {
	return "my string representation"
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

var (
	yamlPath        = arrayFlags{}
	gitlabUrl       string
	gitlabCILintUrl string
	gitlabCIToken   string
	printVersion    bool
	version         = "no-version"
)

func main() {
	flag.Var(&yamlPath, "path", "Path to ci yaml files. Default: current directory")
	flag.StringVar(&gitlabUrl, "url", "https://gitlab.com", "GitLab URL. Default: 'https://gitlab.com'")
	flag.StringVar(&gitlabCILintUrl, "ci-lint-url", "/api/v4/ci/lint", "CI Lint URL. Default: '/api/v4/ci/lint'")
	flag.StringVar(&gitlabCIToken, "gitlab-token", "", "Gitlab token for authentication.")
	flag.BoolVar(&printVersion, "version", false, "Prints printVersion.")
	flag.Parse()

	if printVersion {
		fmt.Printf("CI Linter printVersion: %s", version)
		os.Exit(0)
	}

	client := &http.Client{}

	var yamlFiles = readAllYamlFilePath()

	fmt.Printf("Validate following files: %v\n", yamlFiles)

	ch := make(chan ValidationResult)
	for _, file := range yamlFiles {
		go validate(client, file, ch)
	}

	allValid := true
	for range yamlFiles {
		var result = <-ch
		if !result.ok {
			allValid = false
			fmt.Printf("Invalaid %s file: %s\n", result.yamlFile, result.err)
		}
	}

	if !allValid {
		os.Exit(1)
	}
	fmt.Printf("All files are valid.")
	os.Exit(0)
}

func readAllYamlFilePath() []string {
	var yamlFiles []string
	for _, yamlPath := range yamlPath {
		err := filepath.Walk(yamlPath, func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if info.IsDir() {
				return nil
			}

			if strings.HasSuffix(info.Name(), ".yaml") || strings.HasSuffix(info.Name(), ".yml") {
				yamlFiles = append(yamlFiles, path)
				return nil
			}

			return nil
		})
		if err != nil {
			panic(err)
		}
	}
	return yamlFiles
}

func validate(c *http.Client, yamlFile string, ch chan<- ValidationResult) {
	yamlContent, err := os.ReadFile(yamlFile)
	if err != nil {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       false,
			err:      err,
		}
		return
	}

	var unmarshalledYamlContent interface{}
	err = yaml.Unmarshal(yamlContent, &unmarshalledYamlContent)
	if err != nil {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       false,
			err:      err,
		}
		return
	}

	marshal, err := json.Marshal(unmarshalledYamlContent)
	if err != nil {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       false,
			err:      err,
		}
		return
	}

	marshal, err = json.Marshal(string(marshal))
	if err != nil {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       false,
			err:      err,
		}
		return
	}

	requestBody := []byte(`{"content": ` + string(marshal) + `}`)

	bodyReader := bytes.NewReader(requestBody)
	r, err := http.NewRequest(http.MethodPost, gitlabUrl+gitlabCILintUrl, bodyReader)
	r.Header.Add("Content-Type", "application/json")
	if gitlabCIToken != "" {
		r.Header.Add("PRIVATE-TOKEN", gitlabCIToken)
	}
	resp, err := c.Do(r)
	if err != nil {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       false,
			err:      err,
		}
		return
	}
	defer resp.Body.Close()

	bodyResp, err := io.ReadAll(resp.Body)
	if err != nil {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       false,
			err:      err,
		}
		return
	}

	var validationResponse ValidationResponse
	err = json.Unmarshal(bodyResp, &validationResponse)
	if err != nil {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       false,
			err:      err,
		}
		return
	}

	if validationResponse.Valid {
		ch <- ValidationResult{
			yamlFile: yamlFile,
			ok:       true,
			err:      nil,
		}
		return
	}

	ch <- ValidationResult{
		yamlFile: yamlFile,
		ok:       false,
		err:      fmt.Errorf("%s", validationResponse.Errors),
	}
	return
}
